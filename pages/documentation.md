---
title: Documentation FAQ
keywords: source code repository gitlab
tags: [getting_started, collaboration]
sidebar: home_sidebar
permalink: documentation.html
gitlabedit: /pages/documentation.md
toc: true
comment: true
summary:
---

## How to create or update content? {#create}

The documentation source is available on Gitlab [https://gitlab.cern.ch/webservices/internals-documentation](https://gitlab.cern.ch/webservices/internals-documentation).

You can either use the web interface to manage content or clone the project locally:

```
git clone https://:@gitlab.cern.ch:8443/webservices/internals-documentation.git
```

Content is stored inside the `pages` folder. Inside you will find the list of services and for each service the different procedures.

For example, inside the `eos` folder you will find the file `eos-service.md` with generic service information and all the relevant procedures inside the folders `operations` and `support`. Keep in mind to well structure the content inside your service folder.

```
pages
|-- eos
|   |-- eos-service.md
|   |-- operations
|   |   `-- create-server.md
|   `-- support
|       `-- move-site.md
|-- go-short-url
|   `-- go-service.md
`-- top-level-documentation
    |-- documentation.md
    `-- web-infra-manager.md
```

This site is generated with [Jekyll](https://jekyllrb.com/) and uses [kramdown](https://kramdown.gettalong.org/) as Markdown processor (the same used on [GitHub Pages](https://github.com/blog/2100-github-pages-now-faster-and-simpler-with-jekyll-3-0)). If you are not familiar with kramdown have a look at the [syntax documentation](https://kramdown.gettalong.org/syntax.html). This [Markdown tutorial](http://www.markdowntutorial.com/) is a useful starter.

Once you create your _.md_ file make sure to include some properties at the top of the file (named in the Jekyll world as [FrontMatter](https://jekyllrb.com/docs/frontmatter/)).
This section is identified with the 3 dashes `---` and shown below as an example:

```
---
title: Documentation FAQ
keywords: source code repository gitlab 
tags: [getting_started, collaboration] 
sidebar: home_sidebar 
permalink: documentation.html
gitlabedit: /pages/top-level-documentation/documentation.md
toc: false
summary:
---
```

Most of the properties are self-explanatory, however some comments:

- `keywords`: keywords used by search - make sure you add meaninful keywords to help to find your documentation!
- `tags`: tags allow to nicely group cross service documentation. This is still to be explored further in the future.
- `sidebar`: the navigation bar displayed on the left of your webpage. List of sidebars are available under /_data/sidebars. Each page can have a different sidebar, however aiming to keep the navigation simple the page should use the respective service navigation bar.
- `permalink`: name of the generated HTML file. Be careful with this one because all files are published at the root of the site **without** the folders structure. This is a decision made by the theme developer to make the website fully accessible when run from a local file system (useful if web servers are down!). Thus make sure that the `permalink` property includes the folder structure in the filename, for e.g. for the file `/eos/operations/create-server.md` set as permalink `eos-operations-create-server.html`.
- `gitlabedit`: used to display an `Edit` button at the top of your pages which will directly link to the file on Gitlab. Specify the path to your file e.g. `/pages/documentation.md`.

The navigation files are managed manually, you will need to add your newly created file to the navigation. Please check [How to include a page in the navigation](#navigation) of this FAQ.

Once you created your .md file with respective content, commit your changes on Gitlab and the website will automatically build and publish. You will receive a notification email with the result of the build.

## How to include a page in the navigation? {#navigation}

Files defining the left navigation bar are under the folder `/_data/sidebar/`. Here is an example of a navigation configuration with 3 levels of depth.
```
entries:
- title: Sidebar
  folders:
  - title: EOS Service
    folderitems:
    - title: Home
      url: /eos-service.html
  - title: Operations
    folderitems:
    - title: Create EOS server
      url: /eos-create-server.html
  - title: Support
    folderitems:
    - title: Move EOS site
      url: /eos-move-site.html
      subfolders:
      - title: Migrate EOS site
        subfolderitems:
        - title: to DFS
          url: /eos-move-site1.html
        - title: to OpenShift
          url: /eos-move-site2.html
```
Which will give you:
{% include image.html file="faq/navigation.png" %}

These are YAML configuration files, make sure that you use spaces only (Tabs not supported), indent properties with 1 or more spaces and properties are case-sensitive.

By default the side bar automatically collapse the current `folderitem` when you open a different one. If you want your sidebar to automatically expand each `folderitem` add the property `expand: true` below the sidebar title.

You will get: {% include image.html file="faq/navigation-expanded.png" %}

## How to create a new navigation file? {#newnavigation}

To register a new sidebar:
1. add your _.yml_ file with respective configuration (as above) to `/_data/sidebars`.
2. on the file `/_config.yml` add the sidebar under the `sidebars` property. Make sure to remove the _.yml_ extension.
3. update /_includes/custom/sidebarconfigs.html and add a code block:

```
{% raw %}
{% elsif page.sidebar == "XXX_sidebar" %}
{% assign sidebar = site.data.sidebars.XXX_sidebar.entries %}
{% endraw %}
```
replace `XXX`, before the 3 final lines of the code with:

```
{% raw %}
{% else %}
{% assign sidebar = site.data.sidebars.home_sidebar.entries %}
{% endif %}
{% endraw %}
```


Tedious process but needed to guarantee the full functionality of the website.

## How to access the documentation if the webserver is down?

Two options are available, you can either open the website directly from the underlying filesystem on EOS:

 - From Linux <a href="/eos/project/w/webservices/internals/documentation_web/">/eos/project/w/webservices/internals/documentation_web</a>
 - From Windows <a href='file:///\\cern.ch\eos\project\w\webservices\internals\documentation_web\'>\\\cern.ch\eos\project\w\webservices\internals\documentation_web</a>

Or clone the project to your local computer:
```
git clone https://:@gitlab.cern.ch:8443/webservices/internals-documentation.git
```

## Tips
### Images
About images at [Jekyll Documentation Theme](http://idratherbewriting.com/documentation-theme-jekyll/mydoc_images.html).
Copy your image file to `/images` and then:

```
{% raw %}
{% include image.html file="outline_80_blue.png" %}
{% endraw %}
```

{% include image.html file="outline_80_blue.png" %}


### Icons
The theme uses Font Awesome icons. Look for icons at [http://fontawesome.io/icons/](http://fontawesome.io/icons/) and easily include them in your documentation. For example:

<i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i>
```
<i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i>
```

### Alerts
You can include alerts for info, warnings or others messages like:

{% include note.html content="This is an information note about XYZ." %}

```
{% raw %}
{% include note.html content="This is an information note about XYZ." %}
{% endraw %}
```

{% include warning.html content="This is a warning about XYZ." %}

```
{% raw %}
{% include warning.html content="This is a warning about XYZ." %}
{% endraw %}
```

More details at [http://idratherbewriting.com/documentation-theme-jekyll/mydoc_alerts.html](http://idratherbewriting.com/documentation-theme-jekyll/mydoc_alerts.html)
### Tooltips
Particularly useful to add meta-data to operational procedures. Commonly on documentation we talk about server roles instead of machine names since those can easily change.
However this is an issue for people less involved in the service operation when they need to find/guess on which physical host they should logon to run some command.

Example:
Logon to <a href="#" data-toggle="tooltip" data-original-title="{{site.data.glossary.sharepoint_appserver}}">SharePoint Application Server</a> and run cmd.exe

```
{% raw %}
Logon to <a href="#" data-toggle="tooltip" data-original-title="{{site.data.glossary.sharepoint_appserver}}">SharePoint Application Server</a> and run cmd.exe
{% endraw %}
```

### More
For more have a look at the [Jekyll Documentation Theme Documentation](http://idratherbewriting.com/documentation-theme-jekyll/)
