FROM ruby
MAINTAINER graham@grahamc.com

RUN apt-get update
RUN apt-get install -y node python-pygments

RUN gem install jekyll rdiscount kramdown

VOLUME /src
EXPOSE 4000

WORKDIR /src

CMD jekyll build
ENTRYPOINT ["jekyll", "serve", "--host", "0.0.0.0"]
