# Web Services Internals documentation

## Run straight from gitlab registry

### Linux

- docker run --rm -v `pwd`:/src -p 4000:4000 gitlab-registry.cern.ch/webservices/internals-documentation

### Windows

- docker run --rm -v "$($pwd):/src" -p 4000:4000 gitlab-registry.cern.ch/webservices/internals-documentation

---

### Download the documentation
Clone the project to your local computer with `git clone https://:@gitlab.cern.ch:8443/webservices/internals-documentation.git`
### Build locally
You can build the website locally on your computer before you commit your changes. This is particularly useful to test
and develop new features.
You can either build the site by installing Jekyll in your computer or use the provided Docker image

```
### build the privded image
docker build --no-cache -t jekyll .
### run the container
docker run --rm -v `pwd`:/src -p 4000:4000  jekyll
### on Windows
docker run --rm -v $("$(pwd):/src") -p 4000:4000  jekyll
### open the website with your browser at http://localhost:4000
### run in interactive mode to start and stop Jekyll to rebuild
docker run --rm -v "$($pwd):/src" -p 4000:4000 -it --entrypoint /bin/bash jekyll 
jekyll server -H 0.0.0.0
```
