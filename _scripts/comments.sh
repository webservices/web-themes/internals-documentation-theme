#!/bin/bash

category_id=9
posts=$(curl -X GET --data "api_key=9f945ec92d5cf3dc68fa5030242afed2b2f4916b59107f56ed5b8903619fbf06&api_username=discobot" https://test-web-frameworks-forum.web.cern.ch/c/$category_id.json)
search_dir="pages"
for file in $(find "${search_dir}" -mindepth 1 -maxdepth 5 -type f -print0 | xargs -0 -I {} echo "{}"); do
	# if comments are enabled
	cat $file | grep "comments: true" > /dev/null
	if [ $? -eq 0 ]; then
		# get article title
		title=$(cat $file | grep "title:" -m 1)
		title=${title//title: /}
		# get topic id associated with the article
		id=$(echo $posts | jq -r ".topic_list.topics | map(select(.title == \"${title//\"/\\\"}\")) | .[0].id")
		# echo id: $id, title: \"${title//\"/\\\"}\"
		# if there is no topic
		if [ $id == null ]; then
			id=$(curl -X POST --data "title=$title&raw=Integration with web frameworks website&category=9&api_key=9f945ec92d5cf3dc68fa5030242afed2b2f4916b59107f56ed5b8903619fbf06&api_username=discobot" https://test-web-frameworks-forum.web.cern.ch/posts | jq -r '.topic_id')
		fi
		# associate article with its comments
		sed -i -e "s/comments: true/comments: $id/g" $file
	fi
done
exit 0
